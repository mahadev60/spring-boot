<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Angular Routing</title>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28//angular-route.min.js"></script>
</head>
<body ng-app="myApp" ng-controller="demo">
<a href="#demo1">Demo1</a></br></br>
<a href="#demo2">Demo2</a></br></br>
<div ng-view></div>

<script>
var app = angular.module("myApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "/demo.jsp",
        controller : "demo"
    })
    .when("demo1", {
        templateUrl : "demo1.jsp",
        controller : "demo1"
    })
    .when("demo2", {
        templateUrl : "demo2.jsp",
        controller : "demo2"
    });
    
});
</script>

</body>
</html>