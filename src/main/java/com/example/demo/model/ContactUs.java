package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ContactUs {
  @Id	
  String name;
  String city;
  String subject;
  String phnumber;
  public ContactUs() {}
  
  public String getName() { return name; }
  
  
  public void setName(String name) { this.name = name; }
  
  public String getCity()
  {
    return city;
  }
  
  public void setCity(String city) {
    this.city = city;
  }
  
  public String getSubject() {
    return subject;
  }
  
  public void setSubject(String subject) {
    this.subject = subject;
  }
  
  public String getPhnumber() {
    return phnumber;
  }
  
  public void setPhnumber(String phnumber) {
    this.phnumber = phnumber;
  }
  
  public String toString() {
    return "ContactUs{name=" + name + ", city=" + city + ", subject=" + subject + ", phnumber=" + phnumber + '}';
  }
}