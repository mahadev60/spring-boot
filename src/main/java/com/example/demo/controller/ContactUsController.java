package com.example.demo.controller;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.model.ContactUs;
import com.example.demo.service.ContactUsService;

@Controller
public class ContactUsController {
	   Logger log = Logger.getLogger(ContactUsController.class);
		
		@Autowired
		ContactUsService contactUsService;
		
		@GetMapping("/contactus")
		public String getContactUsPage() throws IOException {
			return "contactus";
		}
		
		@PostMapping("/contactus")
		public String saveContactUs(ContactUs contactUs) throws IOException {
			log.info(contactUs);
			contactUsService.saveContactUs(contactUs);
			return "contactus";
		}
		
}
