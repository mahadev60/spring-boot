package com.example.demo.controller;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GenericWebPages {
    Logger log = Logger.getLogger(GenericWebPages.class);

	@RequestMapping("/")
	public String getWelcomePage() throws IOException {
		return "index";
	}
	
	@RequestMapping("/header")
	public String getHeaderPage() throws IOException {
		return "header";
	}
	
	@RequestMapping("/footer")
	public String getFooterPage() throws IOException {
		return "footer";
	}
	
	@GetMapping("/login")
	public String getLoginPage() throws IOException {
		return "login";
	}
	

	@RequestMapping("/register")
	public String getRegisterPage() throws IOException {
		return "register";
	}

	
	@RequestMapping("/forgotpassword")
	public String getForgotpasswordPage() throws IOException {
		return "forgotpassword";
	}
	
	@RequestMapping("/adminview")
	public String getAdminViewPage() throws IOException {
		return "adminview";
	}
	

	
	@RequestMapping("/demo")
	public String getDemoPage() throws IOException {
		return "Demo";
	}
	
	
	
}
