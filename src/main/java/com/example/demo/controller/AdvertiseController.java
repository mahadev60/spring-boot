package com.example.demo.controller;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Advertise;
import com.example.demo.service.AdvertiseService;

@Controller
public class AdvertiseController {
    Logger log = Logger.getLogger(AdvertiseController.class);

    ModelAndView mv = null;
    
	@Autowired
	AdvertiseService advertiseService;
	
	@GetMapping("/jobadvertise")
	public ModelAndView getJobAdvertisePage() throws Exception {
		
		ModelAndView mv = new ModelAndView("jobadvertise");	
		mv.addObject("adds",advertiseService.getAllAdds());
		return mv;
		
		
	}
	
	@GetMapping("/submitadd")
	public String GetSubmitAddPage() throws Exception {	
	
		return "submitadd";
	}
	
	
	@PostMapping("/submitadd")
	public String SubmitAdd(Advertise advertise) throws Exception {
       try {
		advertiseService.saveAdd(advertise);
		
       }catch(Exception e) {
    	   log.error(e.getLocalizedMessage()+"------"+e.getMessage());   
    	   
       }
	        return "submitadd";
	}
	
	

	@GetMapping("/adddetail")
	public ModelAndView getAddDetailPage(String addid) throws Exception {
		
		mv = new ModelAndView("adddetail");
		mv.addObject("add",advertiseService.getAddById(addid));		
		return mv;
	}

}
