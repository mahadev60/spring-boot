package com.example.demo.dao;

import org.springframework.data.repository.CrudRepository;
import com.example.demo.model.ContactUs;

public interface ContactUsDao extends CrudRepository<ContactUs, String> {

}
